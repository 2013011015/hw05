// draw_shape.cc
#include"draw_shape.h"
#include<vector>
#include<iostream>
#include<string>
using namespace std;
int Canvas::AddShape(const Shape &s)
{
	switch(s.type)
	{
		case  RECTANGLE:
		{
		        if(s.width % 2 == 0 || s.height % 2 == 0)
				return -2;
        		else if(s.x+(s.width/2)>width_ || s.x-(s.width/2)<0 || s.y+(s.height/2)>height_ || s.y-(s.height/2) < 0)
            			return -1;
			else
				shapes_.push_back(s);
			break;
    		}
		case TRIANGLE_UP:
		{
			if(s.y + s.height > height_ || s.x + s.height > width_ || s.x - (s.height-1) < 0)
				return -1; 
			else 
				shapes_.push_back(s);
			break;
    		}
		case TRIANGLE_DOWN:
		{
			if(s.y - (s.height-1) < 0 || s.x + (s.height-1) > width_ || s.x - (s.height-1) < 0 )
				return -1;
			else
				shapes_.push_back(s); 
			break;
		}
	}
}
void Canvas::DeleteShape(int index)
{
	if(index<shapes_.size())
		shapes_.erase(shapes_.begin()+index);
}
void Canvas::Draw(std::ostream& os)
{
	string* canvas = new string [height_];
	
	int i,j,k;
	for (i=0;i<height_;i++)
	{
		for (int j=0;j<width_;j++)
			canvas[i]+='.';
	}
	for (i=0;i<shapes_.size();i++)
	{
		switch(shapes_[i].type)
		{
			case  RECTANGLE:
			{
				for( j= -(shapes_[i].height/2); j < (shapes_[i].height/2)+1 ; j++)
				{
					for( k = -(shapes_[i].width/2); k < (shapes_[i].width/2)+1; k++)
						canvas[(shapes_[i].x)+k][(shapes_[i].y)+j] = shapes_[i].brush;
				}
				break;
    			}
			case TRIANGLE_UP:
			{
				for(j=0;j<shapes_[i].height;j++)
				{
					for(k=-j;k<j+1;k++)
						canvas[shapes_[i].y+j][shapes_[i].x+k]=shapes_[i].brush;
				}
				break;
    			}
			case TRIANGLE_DOWN:
			{
				for(j=0;j<shapes_[i].height;j++)
				{
					for (k=-j;k<j+1;k++)
						canvas[shapes_[i].y-j][shapes_[i].x+k]=shapes_[i].brush;
				}
				break;
			}
		}
	}
	cout<<" ";
	for(i=0;i<width_;i++)
		cout<<i;
	cout << endl;
	for(j=0;j<height_;j++)
		cout<<j<<canvas[j]<<endl;
}
void Canvas::Dump(std::ostream& os)
{
	int i;
	for(i = 0; i < shapes_.size(); i++)
	{
		switch(shapes_[i].type)
		{
			case RECTANGLE:
				os << i << " " << "rect" << " " << shapes_[i].x << " " << shapes_[i].y << " " << shapes_[i].width << " " << shapes_[i].height << " " << shapes_[i].brush << std::endl;
				break;
			case TRIANGLE_UP: 
				os << i << " " << "tri_tp" << " " << shapes_[i].x << " " << shapes_[i].y << " " << shapes_[i].height << " " << shapes_[i].brush << std::endl;
				break;
			case TRIANGLE_DOWN:
				os << i << " " << "tri_down" << " " << shapes_[i].x << " " << shapes_[i].y << " " << shapes_[i].height << " " << shapes_[i].brush << std::endl;
				break;
		}
	}
}
