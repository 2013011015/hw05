// message_book.cc
#include<map>
#include"message_book.h"
#include<string>
#include<iostream>
using namespace std;
void MessageBook::AddMessage(int number, const std::string& message)
{	
	if(messages_.find(number) == messages_.end())
		messages_.insert(make_pair(number,message));
	else if(messages_.find(number) != messages_.end())
		messages_[number] = message;
}
void MessageBook::DeleteMessage(int number)
{
	messages_.erase(number);
}
vector<int> MessageBook::GetNumbers() const
{
	vector<int> phs;
	map<int,string>::const_iterator A;
	for(A=messages_.begin();A!=messages_.end();A++)
		phs.push_back(A->first);
	return phs;
}

const string& MessageBook::GetMessage(int number)const
{
	return messages_.find(number)->second;
}

