// simple_int_set.cc
#include<string>
#include<stdlib.h>
#include"simple_int_set.h"
using namespace std;

set<int> SetIntersection(const set<int>& set0,const set<int>& set1)
{
	set<int>::iterator it;
	set<int>::iterator it_;
	set<int> result;
	for(it = set0.begin(); it != set0.end(); it++)
		for(it_ = set1.begin(); it_ != set1.end(); it_++)
			if(*it == *it_)
				result.insert(*it);
	return result;
}
set<int> SetUnion(const set<int>& set0,const set<int>& set1)
{
	set<int>::iterator it;
	set<int>::iterator it_;
	set<int> result;
	for(it = set0.begin(); it != set0.end(); it++)
	        result.insert(*it);
	for(it_ = set1.begin(); it_ != set1.end(); it_++)
	        result.insert(*it_);
	return result;
}
set<int> SetDifference(const set<int>& set0,const set<int>& set1)
{
	int i=1;
	set<int>::iterator it;
	set<int>::iterator it_;
	set<int> result;
	for(it=set0.begin();it!=set0.end();it++)
	{
		for(it_=set1.begin();it_!=set1.end();it_++)
		{
			if (*it==*it_)
			{
				i=0;
				break;
			}
		}
		if(i==1)
			result.insert(*it);
	}
	return result;
	
}
inline bool IsInt(const string& str)
{
	bool ret = false;
	for (int i = 0; ret == false && i < str.size(); ++i)
	{
		ret = ('0' <= str[i] && str[i] <= '9');
	}
	return ret;
}
bool InputSet(istream& in, set<int>* s)
{
	string str;
	in>>str;
	if (str!="{")
		return false;
	s->clear();
	while(true)
	{
		in>>str;
		if(IsInt(str))
			s->insert(atoi(str.c_str()));			
		else if(str=="}")
			return true;
		else return false;
	}
}
void OutputSet(ostream& out, const set<int>& s)
{
	cout << "{";
	set<int>::iterator it;
	for(it=s.begin();it!=s.end();it++)
		cout<<" "<<*it;
	cout << " }" << endl;
}

