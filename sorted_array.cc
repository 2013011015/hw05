// sorted_array.cc
#include<vector>
#include"sorted_array.h"
#include<algorithm>
using namespace std;
void SortedArray :: AddNumber(int num)
{
     numbers_.push_back(num);
     sort(numbers_.begin(), numbers_.end());
}
std::vector<int> SortedArray::GetSortedAscending() const
{
    return numbers_;    
}

std::vector<int> SortedArray::GetSortedDescending() const
{
    vector<int> A;
    A = numbers_;
    reverse(A.begin(),A.end());
    return A;    
}

int SortedArray::GetMax() const
{
    int max;
    max = numbers_[numbers_.size()-1];
    return max;
}

int SortedArray :: GetMin() const
{
    
    int min;
    min = *numbers_.begin();
    return min;
}
