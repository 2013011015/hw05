// sorted_array_main.cpp

#include <stdlib.h>
#include <iostream>
#include <string>
#include <vector>
#include "sorted_array.h"

using namespace std;

inline bool IsInt(const std::string& str) {
  bool ret = false;
  for (int i = 0; ret == false && i < str.size(); ++i) {
    ret = ('0' <= str[i] && str[i] <= '9');
  }
  return ret;
}

inline ostream& operator<<(ostream& os, const vector<int>& v) {
  if (!v.empty()) os << v[0];
  for (int i = 1; i < v.size(); ++i) os << " " << v[i];
  return os;
}

int main() {
  SortedArray array;
  while (true) {
    string tok;
    cin >> tok;
    if (IsInt(tok)) {
      array.AddNumber(atoi(tok.c_str()));
    } else if (tok == "ascend") {
      cout << array.GetSortedAscending() << endl;
    } else if (tok == "descend") {
      cout << array.GetSortedDescending() << endl;
    } else if (tok == "max") {
      cout << array.GetMax() << endl;
    } else if (tok == "min") {
      cout << array.GetMin() << endl;
    } else {
      break;
    }
  }
  return 0;
}


